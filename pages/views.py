from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum

from envelopes.models import Envelope, Transaction
from accounts.models import Account, Deposit

class HomePageView(LoginRequiredMixin, TemplateView):
    template_name = 'home.html'
    login_url = 'login'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['latest_envelopes'] = Envelope.objects.filter(owner=self.request.user)[:4]
        context['latest_accounts'] = Account.objects.filter(owner=self.request.user)[:4]
        context['total_balance_accounts'] = Account.objects.filter(owner=self.request.user).aggregate(Sum('balance')).get('balance__sum')
        context['total_balance_envelopes'] = Envelope.objects.filter(owner=self.request.user).aggregate(Sum('balance')).get('balance__sum')
        return context

class ProfilePageView(LoginRequiredMixin, TemplateView):
    template_name = 'profile.html'
    login_url = 'login'

class AboutPageView(LoginRequiredMixin, TemplateView):
    template_name = 'about.html'
    login_url = 'login'