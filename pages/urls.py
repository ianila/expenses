from django.urls import path

from .views import HomePageView, ProfilePageView, AboutPageView

urlpatterns = [
    path('profile/', ProfilePageView.as_view(), name='profile'),
    path('about/', AboutPageView.as_view(), name='about'),
    path('', HomePageView.as_view(), name='home')
]