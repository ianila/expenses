from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('users/', include('users.urls')),
    path('users/', include('django.contrib.auth.urls')),
    path('envelopes/', include('envelopes.urls')),
    path('accounts/', include('accounts.urls')),
    path('', include('pages.urls')),

    # api urls
    path('api/', include('api.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)