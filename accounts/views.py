from django.views.generic import ListView, DetailView, CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.db.models import Sum

from .models import Account, Deposit, TransferToAccount, TransferToEnvelope
from .forms import DepositCreateForm, TransferToAccountCreateForm, TransferToEnvelopeCreateForm, AccountUpdateForm

from envelopes.models import Envelope

class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = 'accounts/account_list.html'
    login_url = 'login'

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['total_balance_accounts'] = Account.objects.filter(owner=self.request.user).aggregate(Sum('balance')).get('balance__sum')
        return context        

class AccountDetailView(LoginRequiredMixin, DetailView):
    model = Account
    template_name = 'accounts/account_detail.html'
    login_url = 'login'

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['transfers_to_accounts'] = TransferToAccount.objects.filter(account_from=self.kwargs['pk'])
        context['transfers_to_envelopes'] = TransferToEnvelope.objects.filter(account=self.kwargs['pk'])
        return context  

class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = 'accounts/account_new.html'
    fields = ['name', 'bank', 'balance', 'icon', 'description']
    login_url = 'login'

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)

class AccountUpdateView(LoginRequiredMixin, UpdateView):
    model = Account
    form_class = AccountUpdateForm
    template_name = 'accounts/account_update.html'
    login_url = 'login'

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)

""" ====================================== Deposit ====================================== """

class DepositCreateView(LoginRequiredMixin, CreateView):
    model = Deposit
    template_name = 'accounts/account_new_deposit.html'
    form_class = DepositCreateForm
    login_url = 'login'

    def get_context_data(self, **kwargs):
        context = super(DepositCreateView, self).get_context_data(**kwargs)
        context['form'].fields['account'].queryset = Account.objects.filter(owner=self.request.user)
        context['all_accounts'] = Account.objects.filter(owner=self.request.user)
        return context

    def form_valid(self, form):
        form.instance.owner = self.request.user
        account = Account.objects.get(pk=form.instance.account.id)
        account.balance = form.instance.account.balance + form.instance.amount
        account.save()
        return super(DepositCreateView, self).form_valid(form)

""" ====================================== TransferToAccount ====================================== """

class TransferToAccountCreateView(LoginRequiredMixin, CreateView):
    model = TransferToAccount
    template_name = 'accounts/account_new_transfer_account.html'
    form_class = TransferToAccountCreateForm
    login_url = 'login'

    def get_context_data(self, **kwargs):
        context = super(TransferToAccountCreateView, self).get_context_data(**kwargs)
        context['form'].fields['account_from'].queryset = Account.objects.filter(owner=self.request.user)
        context['form'].fields['account_to'].queryset = Account.objects.filter(owner=self.request.user)
        context['all_accounts'] = Account.objects.filter(owner=self.request.user)
        return context

    def form_valid(self, form):
        form.instance.owner = self.request.user
        account_from = Account.objects.get(pk=form.instance.account_from.id)
        account_to = Account.objects.get(pk=form.instance.account_to.id)
        account_from.balance = form.instance.account_from.balance - form.instance.amount
        account_to.balance = form.instance.account_to.balance + form.instance.amount
        account_from.save()
        account_to.save()
        return super(TransferToAccountCreateView, self).form_valid(form)

""" ====================================== TransferToEnvelope ====================================== """

class TransferToEnvelopeCreateView(LoginRequiredMixin, CreateView):
    model = TransferToEnvelope
    template_name = 'accounts/account_new_transfer_envelope.html'
    form_class = TransferToEnvelopeCreateForm
    login_url = 'login'

    def get_context_data(self, **kwargs):
        context = super(TransferToEnvelopeCreateView, self).get_context_data(**kwargs)
        context['form'].fields['account'].queryset = Account.objects.filter(owner=self.request.user)
        context['form'].fields['envelope'].queryset = Envelope.objects.filter(owner=self.request.user)
        context['all_accounts'] = Account.objects.filter(owner=self.request.user)
        context['all_envelopes'] = Envelope.objects.filter(owner=self.request.user)
        return context

    def form_valid(self, form):
        form.instance.owner = self.request.user
        account = Account.objects.get(pk=form.instance.account.id)
        envelope = Envelope.objects.get(pk=form.instance.envelope.id)
        account.balance = form.instance.account.balance - form.instance.amount
        envelope.balance = form.instance.envelope.balance + form.instance.amount
        account.save()
        envelope.save()
        return super(TransferToEnvelopeCreateView, self).form_valid(form)