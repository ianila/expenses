from django.forms import ModelForm
from django import forms

from .models import Deposit, TransferToAccount, TransferToEnvelope, Account

class AccountUpdateForm(ModelForm):
    class Meta:
        model = Account
        fields =  ['name', 'bank', 'icon', 'description']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'account__new-input'}),
            'bank': forms.TextInput(attrs={'class': 'account__new-input'}),
            'description': forms.Textarea(attrs={'class': 'account__new-input', 'cols': 0, 'rows': 3}),
            'icon': forms.Select(attrs={'class': 'account__new-input'}),
        }
        labels = {
            'name': '',
            'bank': '',
            'description': '',
            'icon': '',
        }

class DepositCreateForm(ModelForm):
    class Meta:
        model = Deposit
        fields = '__all__'
        widgets = {
            'account': forms.Select(attrs={'class': 'account__new-input'}),
        }

class TransferToAccountCreateForm(ModelForm):
    class Meta:
        model = TransferToAccount
        fields = '__all__'
        widgets = {
            'account_from': forms.Select(attrs={'class': 'account__new-input'}),
            'account_to': forms.Select(attrs={'class': 'account__new-input'}),
        }

class TransferToEnvelopeCreateForm(ModelForm):
    class Meta:
        model = TransferToEnvelope
        fields = '__all__'
        widgets = {
            'account': forms.Select(attrs={'class': 'account__new-input'}),
            'envelope': forms.Select(attrs={'class': 'account__new-input'}),
        }