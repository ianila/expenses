from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from envelopes.models import Envelope

class Account(models.Model):
    class AccountIcon(models.TextChoices):
        CREDITCARD = 'far fa-credit-card', _('Credit Card')
        SAVINGS = 'fas fa-piggy-bank', _('Savings')
        FIXED = 'fas fa-money-check-alt', _('Fixed')  

    name = models.CharField(max_length=255)
    bank = models.CharField(max_length=255, null=True, blank=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    balance = models.DecimalField(decimal_places=2, max_digits=12, default=0.0)
    icon = models.CharField(
        max_length=50,
        choices=AccountIcon.choices,
    )
    owner = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('account_list')

class Deposit(models.Model):
    amount = models.DecimalField(decimal_places=2, max_digits=10)
    date = models.DateTimeField(auto_now_add=True)
    comment = models.CharField(max_length=255, null=True, blank=True)
    account = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse('account_detail', kwargs={'pk': self.account.pk,})

class TransferToAccount(models.Model):
    amount = models.DecimalField(decimal_places=2, max_digits=10)
    date = models.DateTimeField(auto_now_add=True)
    comment = models.CharField(max_length=255, null=True, blank=True)
    account_from = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
        related_name='account_from',
    )
    account_to = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
        related_name='account_to',
    )

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse('account_list')

class TransferToEnvelope(models.Model):
    amount = models.DecimalField(decimal_places=2, max_digits=10)
    date = models.DateTimeField(auto_now_add=True)
    comment = models.CharField(max_length=255, null=True, blank=True)
    account = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
    )
    envelope = models.ForeignKey(
        Envelope,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse('account_list')