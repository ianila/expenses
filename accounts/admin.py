from django.contrib import admin

from .models import Account, Deposit, TransferToAccount

admin.site.register(Account)
admin.site.register(Deposit)
admin.site.register(TransferToAccount)