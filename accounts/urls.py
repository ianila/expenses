from django.urls import path

from .views import (
    AccountListView, 
    AccountDetailView, 
    AccountCreateView,
    AccountUpdateView,
    DepositCreateView, 
    TransferToAccountCreateView, 
    TransferToEnvelopeCreateView,
)

urlpatterns = [
    path('<int:pk>/', AccountDetailView.as_view(), name='account_detail'),
    path('new/', AccountCreateView.as_view(), name='account_new'),
    path('new/deposit/', DepositCreateView.as_view(), name='account_new_deposit'),
    path('new/transfertoaccount/', TransferToAccountCreateView.as_view(), name='account_new_account_transfer'),
    path('new/transfertoenvelope/', TransferToEnvelopeCreateView.as_view(), name='account_new_envelope_transfer'),
    path('<pk>/update', AccountUpdateView.as_view(), name='account_update'),
    path('', AccountListView.as_view(), name='account_list'),
]