from django.urls import path

from . import views

urlpatterns = [
    path('accounts/', views.AccountsList.as_view()),
]