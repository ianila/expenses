from rest_framework import generics, permissions

from .serializers import AccountSerializer
from accounts.models import Account

class AccountsList(generics.ListAPIView):
    serializer_class = AccountSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)