from rest_framework import serializers

from accounts.models import Account

class AccountSerializer(serializers.ModelSerializer):
    balance = serializers.ReadOnlyField()
    
    class Meta:
        model = Account
        fields = ['id', 'name', 'bank', 'description', 'balance', 'icon']