from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

class Envelope(models.Model):
    class EnvelopeIcon(models.TextChoices):
        GROCERIES = 'fas fa-shopping-basket', _('Groceries')
        FOODANDDRINKS = 'fas fa-coffee', _('Food and Drinks')
        ENTERTAINMENT = 'fas fa-compact-disc', _('Entertainment')
        FURNITURE = 'fas fa-couch', _('Furniture')
        # CREDITCARD = 'far fa-credit-card', _('Credit Card')
        SPORTS = 'fas fa-dice', _('Sports')
        MEDICINE = 'fas fa-hospital', _('Medicine')
        MOBILE = 'fas fa-mobile-alt', _('Mobile Phone')
        # SAVINGS = 'fas fa-piggy-bank', _('Savings')
        SHOPPING = 'fas fa-store', _('Shopping')
        UTILITIES = 'fas fa-tools', _('Utilities')
        CLOTHES = 'fas fa-tshirt', _('Clothes')
        BABYCARE = 'fas fa-baby-carriage', _('Baby Care')
        BOOKS = 'fas fa-book', _('Books')
        TRANSPORTATION = 'fas fa-bus', _('Transportation')
        GIFTS = 'fas fa-box-open', _('Gifts')
        TRAVEL = 'fas fa-car', _('Travel')
        TOOLS = 'fas fa-hammer', _('Tools')
        INTERNET = 'fas fa-wifi', _('Internet')
        INSURANCE = 'fas fa-car-crash', _('Insurance')
        WATER = 'fas fa-water', _('Water')
        BILLS = 'fas fa-file-invoice', _('Bills')
        FUEL = 'fas fa-gas-pump', _('Fuel')
        ELECTRICITY = 'far fa-lightbulb', _('Electricity')
        MISCELLANEOUS = 'fas fa-cannabis', _('Miscellaneous')
        

    name = models.CharField(max_length=255)
    balance = models.DecimalField(decimal_places=2, max_digits=8, default=0.0)
    description = models.CharField(max_length=255, null=True, blank=True)
    icon = models.CharField(
        max_length=30,
        choices=EnvelopeIcon.choices,
    )
    owner = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('envelope_list')

class Spend(models.Model):
    amount = models.DecimalField(decimal_places=2, max_digits=8)
    date = models.DateTimeField(auto_now_add=True)
    comment = models.CharField(max_length=255, null=True, blank=True)
    envelope = models.ForeignKey(
        Envelope,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse('envelope_detail', kwargs={'pk': self.envelope.pk,})

class Transaction(models.Model):
    class TransactionCategory(models.TextChoices):
        INCOME = 'IN', _('Income')
        EXPENSE = 'EX', _('Expense')

    amount = models.DecimalField(decimal_places=2, max_digits=8)
    date = models.DateTimeField(auto_now_add=True)
    category = models.CharField(
        max_length=2,
        choices=TransactionCategory.choices,
    )
    envelope = models.ForeignKey(
        Envelope,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse('envelope_detail', kwargs={'pk': self.envelope.pk,})