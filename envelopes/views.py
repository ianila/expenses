from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.db.models import Sum

from .models import Envelope, Transaction, Spend
from .forms import TransactionCreateForm, SpendCreateForm

from accounts.models import TransferToEnvelope, Account

class EnvelopeListView(LoginRequiredMixin, ListView):
    model = Envelope
    template_name = 'envelopes/envelope_list.html'
    login_url = 'login'

    def get_queryset(self):
        return Envelope.objects.filter(owner=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['total_balance_envelopes'] = Envelope.objects.filter(owner=self.request.user).aggregate(Sum('balance')).get('balance__sum')
        context['all_accounts'] = Account.objects.filter(owner=self.request.user)
        return context 

class EnvelopeDetailView(LoginRequiredMixin, DetailView):
    model = Envelope
    template_name = 'envelopes/envelope_detail.html'
    login_url = 'login'

    def get_queryset(self):
        return Envelope.objects.filter(owner=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['transfers_to_envelopes'] = TransferToEnvelope.objects.filter(envelope=self.kwargs['pk'])
        return context 

class EnvelopeCreateView(LoginRequiredMixin, CreateView):
    model = Envelope
    template_name = 'envelopes/envelope_new.html'
    fields = ['name', 'balance', 'icon', 'description']
    login_url = 'login'

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['all_accounts'] = Account.objects.filter(owner=self.request.user)
        return context

""" ====================================== Spend ====================================== """

class SpendCreateView(LoginRequiredMixin, CreateView):
    model = Spend
    template_name = 'envelopes/envelope_new_spend.html'
    form_class = SpendCreateForm
    login_url = 'login'

    def get_context_data(self, **kwargs):
        context = super(SpendCreateView, self).get_context_data(**kwargs)
        context['form'].fields['envelope'].queryset = Envelope.objects.filter(owner=self.request.user)
        context['all_envelopes'] = Envelope.objects.filter(owner=self.request.user)
        return context

    def form_valid(self, form):
        form.instance.owner = self.request.user
        envelope = Envelope.objects.get(pk=form.instance.envelope.id)
        envelope.balance = form.instance.envelope.balance - form.instance.amount
        envelope.save()
        return super(SpendCreateView, self).form_valid(form)


""" ====================================== Transactions ====================================== """

class TransactionCreateView(LoginRequiredMixin, CreateView):
    model = Transaction
    template_name = 'envelopes/transaction_new.html'
    form_class = TransactionCreateForm
    #fields = ['envelope', 'category', 'amount']
    login_url = 'login'

    def get_context_data(self, **kwargs):
        context = super(TransactionCreateView, self).get_context_data(**kwargs)
        context['form'].fields['envelope'].queryset = Envelope.objects.filter(owner=self.request.user)
        return context

    def form_valid(self, form):
        form.instance.owner = self.request.user
        envelope = Envelope.objects.get(pk=form.instance.envelope.id)
        
        if(form.instance.category == 'IN'):
            envelope.balance = form.instance.envelope.balance + form.instance.amount
        else:
            envelope.balance = form.instance.envelope.balance - form.instance.amount
        envelope.save()
        return super(TransactionCreateView, self).form_valid(form)