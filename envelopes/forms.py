from django.forms import ModelForm
from django import forms

from .models import Transaction, Spend

class SpendCreateForm(ModelForm):
    class Meta:
        model = Spend
        fields = '__all__'
        widgets = {
            'envelope': forms.Select(attrs={'class': 'account__new-input'}),
        }

class TransactionCreateForm(ModelForm):
    class Meta:
        model = Transaction
        fields = '__all__'
        widgets = {
            'envelope': forms.Select(attrs={'class': 'px-3 py-3 placeholder-gray-400 text-gray-700 bg-gray-200 rounded text-sm shadow focus:outline-none focus:shadow-outline w-full appearance-none'}),
            'category': forms.Select(attrs={'class': 'px-3 py-3 placeholder-gray-400 text-gray-700 bg-gray-200 rounded text-sm shadow focus:outline-none focus:shadow-outline w-full appearance-none'}),
        }