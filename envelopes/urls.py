from django.urls import path

from .views import EnvelopeListView, EnvelopeDetailView, EnvelopeCreateView
from .views import TransactionCreateView, SpendCreateView

urlpatterns = [
    path('<int:pk>/', EnvelopeDetailView.as_view(), name='envelope_detail'),
    path('newspend/', SpendCreateView.as_view(), name='envelope_new_spend'),
    path('newtransaction/', TransactionCreateView.as_view(), name='transaction_new'),
    path('new/', EnvelopeCreateView.as_view(), name='envelope_new'),
    path('', EnvelopeListView.as_view(), name='envelope_list'),
]