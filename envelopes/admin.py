from django.contrib import admin

from .models import Envelope, Transaction

admin.site.register(Envelope)
admin.site.register(Transaction)