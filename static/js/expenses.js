/*===== MENU SHOW Y HIDDEN =====*/

// SHOW
document.getElementById('nav-toggle').addEventListener('click', () => {
    document.getElementById('nav-menu').classList.toggle('show');
})

// HIDDEN
document.getElementById('nav-close').addEventListener('click', () => {
    document.getElementById('nav-menu').classList.remove('show');
})

/*===== ACTIVE AND REMOVE MENU =====*/
document.querySelectorAll('.nav__link').forEach(n => n.addEventListener('click', () => {
    document.getElementById('nav-menu').classList.remove('show');
}))