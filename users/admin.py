from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import ExpenseUserCreationForm, ExpenseUserChangeForm
from .models import ExpenseUser

class ExpenseUserAdmin(UserAdmin):
    add_form = ExpenseUserCreationForm
    form = ExpenseUserChangeForm
    model = ExpenseUser    

admin.site.register(ExpenseUser, ExpenseUserAdmin)