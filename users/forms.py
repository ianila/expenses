from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import ExpenseUser

class ExpenseUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = ExpenseUser
        fields = UserCreationForm.Meta.fields + ('age', 'profile_pic')

class ExpenseUserChangeForm(UserChangeForm):
    class Meta:
        model = ExpenseUser
        fields = UserChangeForm.Meta.fields