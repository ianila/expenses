from django.contrib.auth.models import AbstractUser
from django.db import models

class ExpenseUser(AbstractUser):
    age = models.PositiveIntegerField(null=True, blank=True)
    profile_pic = models.ImageField(null=True, blank=True)